// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#include "jbmm.h"
#include <iostream>
#include <sstream>
#include <stdexcept>

namespace jbmm {

  matrix::matrix(const unsigned int lines, const unsigned int cols)
  {
    this->lines = lines;
    this->cols = cols;
    this->n_words_line = (cols / this->bit_p_word) +
      ((cols % this->bit_p_word) ? 1 : 0);
    this->bm = new unsigned int[this->n_words_line * lines];
  }

  inline void matrix::valid_pos(const unsigned int l,
                                const unsigned int c) const
  {
    std::stringstream ss;
    if(l >= this->lines)
    {
      ss << "Line out of range [0.." << (this->cols - 1) << "]";
      throw new std::out_of_range(ss.str());
    }
    if(c >= this->cols)
    {      
      ss << "Column out of range [0.." << (this->lines - 1) << "]";
      throw new std::out_of_range(ss.str());
    }
  }

  inline unsigned int matrix::word_mask(const unsigned int pos) const
  {
    unsigned int sf = ((this->bit_p_word - 1) - (pos % this->bit_p_word));
    unsigned int one = 1;
    return one << sf;
  }

  inline unsigned int matrix::word_pos(const unsigned int l,
                                       const unsigned int c) const
  {
    unsigned int pos = l * this->n_words_line; // line
    pos += (c / this->bit_p_word); // col
    return pos;
  }

  bool matrix::get_bit(const unsigned int l, const unsigned int c) const
  {
    const unsigned int pos = this->word_pos(l, c);
    return (this->bm[pos] & this->word_mask(c));
  }

  void matrix::set_bit(const unsigned int l, const unsigned int c)
  {
    this->valid_pos(l, c); 
    unsigned int pos = this->word_pos(l, c);
    this->bm[pos] |= this->word_mask(c);
  }

  void matrix::reset_bit(const unsigned int l, const unsigned int c)
  {
    this->valid_pos(l, c); 
    unsigned int pos = this->word_pos(l, c);
    unsigned int sf = ((this->bit_p_word - 1) - (c % this->bit_p_word));
    this->bm[pos] ^= this->word_mask(c);
    
    if((this->bm[pos] >> sf) & 1) {
      this->bm[pos] ^= this->word_mask(c);
    }
  }

  void matrix::stdout_print() const
  {
    for(unsigned int i = 0; i < this->lines; i++)
    {
      for(unsigned int j = 0; j < this->cols; j++)
      {
        (this->get_bit(i, j) == 1) ? std::cout << "X" : std::cout << "0";
      }
      std::cout << std::endl;
    }
  }

  matrix::~matrix()
  {
    delete(this->bm);
  }
}
