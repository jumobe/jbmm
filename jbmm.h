// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-

#ifndef JBMM_CLASS_H
#define JBMM_CLASS_H

namespace jbmm {
  class matrix {
    
  private:
    unsigned int lines;
    unsigned int cols;
    const unsigned int bit_p_word = sizeof(unsigned int) * 8;
    unsigned int n_words_line;
    unsigned int *bm;

    inline void valid_pos(const unsigned int, const unsigned int) const;
    inline unsigned int word_mask(const unsigned int) const;
    inline unsigned int word_pos(const unsigned int, const unsigned int) const;

  public:
    explicit matrix(const unsigned int, const unsigned int);
    ~matrix();
    void set_bit(const unsigned int, const unsigned int);
    void reset_bit(const unsigned int, const unsigned int);
    bool get_bit(const unsigned int, const unsigned int) const;
    void stdout_print() const;
    
  };
}

#endif
