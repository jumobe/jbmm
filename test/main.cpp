// -*- mode:c++;c-file-style:"linux";indent-tabs-mode:nil;c-basic-offset:2 -*-


#include <iostream>
#include <jbmm.h>

int main(int argc, char **argv)
{
  if (argc < 3)
  {
    return 1;
  }
  unsigned int lines = std::stoi(argv[1]);
  unsigned int cols = std::stoi(argv[2]);

  jbmm::matrix m(lines, cols);
  m.set_bit(2,0);
  m.set_bit(2,10);
  m.set_bit(10,11);
  m.reset_bit(2,10);
  m.reset_bit(2,11);
  m.stdout_print();
  std::cout << m.get_bit(2,11) << std::endl;
  std::cout << m.get_bit(2,0) << std::endl;
  
  return 0;
}
